<header class="banner">
  <div class="logo">
    <a class="brand" href="{{ home_url('/') }}" alt="{{ get_bloginfo('name', 'display') }}">
      <img src="@asset('images/FordhamHS-Logo.png')" alt="Fordham High School for the Arts logo" />
    </a>
  </div>
  <nav class="nav-primary desktop">
    @if (has_nav_menu('primary_navigation'))
      {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
    @endif
  </nav>

  {{-- Search Icon --}}
  <div class="search toggle desktop">
    <i class="fas fa-search"></i>
  </div>

  {{-- Hamburger --}}
  <div class="hamburger"> 
    <i class="fas fa-bars"></i> 
  </div> 
</header>

<div class="search">@php get_search_form() @endphp </div>

<nav class="nav-primary mobile column">
    @if (has_nav_menu('primary_navigation'))
      {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
    @endif
  </nav>
