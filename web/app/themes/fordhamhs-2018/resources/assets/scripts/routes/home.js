export default {
  init() {
    // JavaScript to be fired on the home page

    const source = $('.video-player iframe').attr('source');
    
    $('.video-btn').click(function () { 
      $('.video-player').addClass('active');
      $('.video-player iframe').attr('src', source)
    })
    
    $('.video-player').click(function () { 
      $('.video-player').removeClass('active');
      $('.video-player iframe').attr('src', '')
    })
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
